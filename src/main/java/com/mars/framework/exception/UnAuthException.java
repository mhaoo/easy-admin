package com.mars.framework.exception;

import com.mars.common.enums.HttpCodeEnum;

/**
 * 未授权异常
 *
 * @author 源码字节-程序员Mars
 */
public class UnAuthException extends RuntimeException {

    private String code;

    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UnAuthException(String msg) {
        this.code = HttpCodeEnum.ERROR.getCode();
        this.msg = msg;
    }

    public UnAuthException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
