package com.mars.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 设备类型
 * 针对多套 用户体系
 *
 * @author 源码字节-程序员Mars
 */
public enum UserTypeEnums {

    /**
     * 系统内置用户
     */
    SYS_USER(0, "sys_user", "系统内置用户"),
    /**
     * 普通注册用户
     */
    COMMON_USER(1, "common", "普通注册用户");

    private Integer type;

    private String code;

    private String message;

    UserTypeEnums() {
    }

    UserTypeEnums(Integer type, String code, String message) {
        this.type = type;
        this.code = code;
        this.message = message;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
