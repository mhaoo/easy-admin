package com.mars.common.enums;


import lombok.Getter;

@Getter
public enum ActionEnum {

    A_SUBMIT("submit", "提交", 10),
    A_REJECT("reject", "驳回", 20),
    A_RECALL("recall", "撤回", 30),
    A_TURNOFF("turnOff", "撤转", 40),
    A_CANCLE("cancle", "作废", 50),
    A_TRANSFER("transfer", "转办", 60),
    A_TRANSFER_RECALL("transfer_recall", "撤回", 30),
    A_DELEGATE("delegate", "委托", 70),
    A_DELEGATE_SUBMIT("delegate_submit", "委托处理", 80),
    A_COMMUNICATE("communicate", "沟通", 90),
    A_COMMUNICATE_BACK("communicate_back", "沟通反馈", 100),
    A_COMMUNICATE_END("communicate_end", "终止沟通", 110),
    A_COMMUNITY_TRANSFER("communityTransfer", "转办", 60),
    A_COMMUNITY_TRANSFER_RECALL("communityTransferRecall", "撤回", 30),
    A_NOTIFY("notify", "知会", 120),
    A_SIGN("sign", "会签", 130),
    A_SIGNADD("signAdd", "加签", 140),
    A_SIGNMIN("signMin", "减签", 150),
    A_VIEW("view", "查看流程", 160),
    A_START("start", "启动流程"),
    A_END("end", "流程结束"),
    A_NOTIFY_BACK("notify_back", "打开待阅"),
    A_GET_PROC_POWER("get_proc_power", "加载流程权限信息"),
    A_GET_PROC_ALL("get_proc_all", "加载完整流程信息"),
    A_GET_NEXT_NODE("get_next_node", "获取一下环节"),
    A_GET_TURNOFF_NODE("get_turnoff_node", "获取撤转环节"),
    A_GET_REJECT_NODE("get_reject_node", "获取驳回环节"),
    A_TODO("todo", "获取待办"),
    A_DONE("done", "获取已办"),
    A_UNREAD("unread", "获取待阅"),
    A_READ("read", "获取已阅");

    private String code;

    private String desc;
    //操作按钮排序号
    private Integer sortNum;

    private ActionEnum(String code, String desc, Integer sortNum) {
        this.code = code;
        this.desc = desc;
        this.sortNum = sortNum;
    }

    private ActionEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * @param code
     * @return
     * @throws IllegalArgumentException
     * @Description 将给定的字符串code转成对应的枚举对象
     */
    public static ActionEnum resolve(String code) throws IllegalArgumentException {
        for (ActionEnum c : ActionEnum.values()) {
            if (c.code.equalsIgnoreCase(code)) {
                return c;
            }
        }
        throw new IllegalArgumentException("未定义的ActionEnum: " + code);
    }

    /**
     * @param name
     * @return
     * @throws IllegalArgumentException
     * @Description 将给定的字符串名称转成对应的枚举对象
     */
    public static ActionEnum resolveName(String name) throws IllegalArgumentException {
        for (ActionEnum c : ActionEnum.values()) {
            if (c.desc.equalsIgnoreCase(name)) {
                return c;
            }
        }
        throw new IllegalArgumentException("未定义的ActionEnum: " + name);
    }
}
