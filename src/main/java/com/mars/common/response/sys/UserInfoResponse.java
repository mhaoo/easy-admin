package com.mars.common.response.sys;


import com.mars.module.system.entity.SysRole;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 登录VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class UserInfoResponse {

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "用户信息")
    private SysUserDetailResponse sysUser;

    @ApiModelProperty(value = "菜单菜单")
    private List<SysMenuResponse> sysMenu;

    @ApiModelProperty(value = "角色列表")
    private List<SysRole> sysRoles;

    /**
     * 角色列表
     */
    private String sysRolesName;


}
