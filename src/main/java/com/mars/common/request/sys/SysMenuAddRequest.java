package com.mars.common.request.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 菜单新增DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysMenuAddRequest {

    @NotEmpty
    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "URL")
    private String url;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "权限标识")
    private String perms;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "菜单类型 1目录 2 菜单 3 按钮")
    private Integer menuType;


}
