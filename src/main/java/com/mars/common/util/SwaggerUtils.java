package com.mars.common.util;

import com.mars.common.request.tool.ApiParamRequest;
import com.mars.common.request.tool.ApiRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Stream;

/**
 * Swagger工具类
 *
 * @author 源码字节-程序员Mars
 */
public class SwaggerUtils {

    /**
     * 封装swagger接口数据
     *
     * @param api
     * @return
     */
    public static List<ApiRequest> getSwaggerData(String api) {
        List<ApiRequest> list = new ArrayList<>();
        Map<String, Object> swaggerApiMap = new RestTemplate().getForObject(api + "/v3/api-docs", Map.class);
        Map<String, Map<String, Object>> paths = (Map<String, Map<String, Object>>) swaggerApiMap.get("paths");
        if (paths == null) {
            return list;
        }
        Map<String, Map<String, Object>> definitions = (Map<String, Map<String, Object>>) ((Map<String, Object>) swaggerApiMap.get("components")).get("schemas");
        Iterator<Map.Entry<String, Map<String, Object>>> pathIterator = paths.entrySet().iterator();
        while (pathIterator.hasNext()) {
            Map.Entry<String, Map<String, Object>> path = pathIterator.next();
            String url = path.getKey();
            Iterator<Map.Entry<String, Object>> methodIterator = path.getValue().entrySet().iterator();
            while (methodIterator.hasNext()) {
                Map.Entry<String, Object> methodEntry = methodIterator.next();
                Map<String, Object> methodContent = (Map<String, Object>) methodEntry.getValue();
                ApiRequest apiRequest = new ApiRequest();
                apiRequest.setUrl(url);
                apiRequest.setMethod(methodEntry.getKey());
                apiRequest.setTitle(String.valueOf(methodContent.get("summary")));
                apiRequest.setTags(String.valueOf(((List) methodContent.get("tags")).get(0)));
                apiRequest.setRequestList(getRequestList(methodContent, definitions));
                apiRequest.setResponseList(getResponseList(methodContent, definitions));
                list.add(apiRequest);
            }
        }
        return list;
    }

    /**
     * 获取请求参数
     *
     * @param methodContent
     * @param definitions
     * @return
     */
    private static List<ApiParamRequest> getRequestList(Map<String, Object> methodContent, Map<String, Map<String, Object>> definitions) {
        List<ApiParamRequest> requestList = new ArrayList<>();
        List<LinkedHashMap> parameters = (ArrayList) methodContent.get("parameters");
        Map<String, Object> requestBody = (Map<String, Object>) methodContent.get("requestBody");
        if (parameters != null) {
            for (Map<String, Object> parameter : parameters) {
                Map<String, Object> schema = (Map<String, Object>) parameter.get("schema");
                if (schema == null) {
                    ApiParamRequest apiParamRequest = new ApiParamRequest();
                    apiParamRequest.setName((String) parameter.get("name"));
                    apiParamRequest.setType((String) parameter.get("type"));
                    apiParamRequest.setComment((String) parameter.get("description"));
                    apiParamRequest.setRequired((boolean) parameter.get("required") ? "是" : "否");
                    requestList.add(apiParamRequest);
                } else {
                    String ref = (String) schema.get("$ref");
                    String type = (String) schema.get("type");
                    String originalRef = ref != null ? ref.substring(ref.lastIndexOf("/") + 1) : null;
                    if (originalRef != null) {
                        analyzingRef(definitions, originalRef, requestList, null, true);
                    }
                    if (type != null) {
                        ApiParamRequest apiParamRequest = new ApiParamRequest();
                        apiParamRequest.setName((String) parameter.get("name"));
                        apiParamRequest.setType(type);
                        apiParamRequest.setComment((String) parameter.get("description"));
                        apiParamRequest.setRequired((boolean) parameter.get("required") ? "是" : "否");
                        requestList.add(apiParamRequest);
                    }
                }
            }
        }
        if (requestBody != null) {
            Map<String, Object> schema = (Map<String, Object>) ((Map<String, Object>) ((Map<String, Object>) requestBody.get("content")).get("application/json")).get("schema");
            String ref = (String) schema.get("$ref");
            String originalRef = ref != null ? ref.substring(ref.lastIndexOf("/") + 1) : null;
            if (originalRef != null) {
                analyzingRef(definitions, originalRef, requestList, null, true);
            }
        }
        return requestList;
    }

    /**
     * 获取响应参数
     *
     * @param methodContent
     * @param definitions
     * @return
     */
    private static List<ApiParamRequest> getResponseList(Map<String, Object> methodContent, Map<String, Map<String, Object>> definitions) {
        List<ApiParamRequest> responseList = new ArrayList<>();
        Map<String, Object> responses = (Map<String, Object>) methodContent.get("responses");
        Map<String, Object> responses200 = (Map<String, Object>) responses.get("200");
        Map<String, Object> content = (Map<String, Object>) responses200.get("content");
        if (content == null) {
            return responseList;
        }
        Map<String, Object> schema = (Map<String, Object>) ((Map<String, Object>) content.get("*/*")).get("schema");
        if (schema != null) {
            String ref = (String) schema.get("$ref");
            String originalRef = ref != null ? ref.substring(ref.lastIndexOf("/") + 1) : null;
            if (originalRef != null) {
                analyzingRef(definitions, originalRef, responseList, null, false);
            }
        }
        return responseList;
    }

    /**
     * 解析对象
     *
     * @param definitions
     * @param originalRef
     * @param paramList
     * @param name
     * @param required
     */
    public static void analyzingRef(Map<String, Map<String, Object>> definitions, String originalRef, List<ApiParamRequest> paramList, String name, boolean required) {
        if (name != null) {
            String[] arr = name.split("\\.");
            int level = 4;
            if (Stream.of(arr).distinct().count() < arr.length || arr.length == level) {
                return;
            }
        }
        Map<String, Object> obj = definitions.get(originalRef);
        if (obj != null) {
            List<String> requiredList = (List<String>) obj.get("required");
            Map<String, Object> properties = (Map<String, Object>) obj.get("properties");
            if (properties != null) {
                Iterator<Map.Entry<String, Object>> propertiesIterator = properties.entrySet().iterator();
                while (propertiesIterator.hasNext()) {
                    Map.Entry<String, Object> propertiesEntry = propertiesIterator.next();
                    Map<String, Object> property = (Map<String, Object>) propertiesEntry.getValue();
                    ApiParamRequest apiParamRequest = new ApiParamRequest();
                    if (!StringUtils.hasText(name)) {
                        apiParamRequest.setName(propertiesEntry.getKey());
                    } else {
                        apiParamRequest.setName(name + "." + propertiesEntry.getKey());
                    }
                    apiParamRequest.setType((String) property.get("type"));
                    apiParamRequest.setComment((String) property.get("title"));
                    if (required) {
                        apiParamRequest.setRequired(requiredList != null && requiredList.contains(apiParamRequest.getName()) ? "是" : "否");
                    }
                    paramList.add(apiParamRequest);
                    String propertyRef = (String) property.get("$ref");
                    String dataOriginalRef = propertyRef != null ? propertyRef.substring(propertyRef.lastIndexOf("/") + 1) : null;
                    if (dataOriginalRef == null) {
                        Map<String, Object> items = (Map<String, Object>) property.get("items");
                        if (items != null) {
                            String itemsRef = (String) items.get("$ref");
                            String itemsOriginalRef = itemsRef != null ? itemsRef.substring(itemsRef.lastIndexOf("/") + 1) : null;
                            analyzingRef(definitions, itemsOriginalRef, paramList, apiParamRequest.getName(), required);
                        }
                    } else {
                        apiParamRequest.setType("object");
                        analyzingRef(definitions, dataOriginalRef, paramList, apiParamRequest.getName(), required);
                    }
                }
            }
        }
    }
}
